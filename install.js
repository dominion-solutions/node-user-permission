#!/usr/bin/env node
const inquirer = require('inquirer');
const fs = require('fs-extra');
inquirer.prompt([
    {
        name: 'migrationTarget',
        message: 'Migration Directory:',
        default: 'src/migration',
    },
    {
        type: 'list',
        name: 'es',
        message: 'Use ES?',
        choices: ['Yes', 'No'],
        default: 'No',
    }
]).then((answers) => {
    let migrationTarget = process.env.INIT_CWD + answers.migrationTarget;
    let es = (answers.es === 'Yes');
    let migrationSrc = es ? `${__dirname}/lib/cjs/migration` : `${__dirname}/lib/es/migration`
    fs.copySync(migrationSrc, migrationTarget, (err) => {
        if (err) {
            console.log(err);
        }
        else {
            console.log("Successfully Copied Migrations");
        }
    })
}).catch((reason) => {
    console.error(reason);
});

