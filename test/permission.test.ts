import { Connection, ConnectionOptions, createConnection, Equal, getConnection, getConnectionOptions } from 'typeorm';
import * as faker from 'faker';
import * as _ from 'lodash';

import { Permission } from '../src/entity/Permission';
import { Role } from '../src/entity/Role';


var connection: Connection;

const PERMISSION_ACTIONS = ['create:any', 'create:own', 'read:any', 'read:own', 'update:any', 'update:own', 'delete:any', 'delete:own'];

beforeAll(async() =>
{
    connection = await createConnection({
        type: "sqlite",
        database: ":memory:",
        migrations: ['src/migration/*.ts'],
        entities: ['src/entity/*.ts'],
        migrationsRun: true
    });
});

it("Creates Permission", async () => {
    const permission = new Permission();
    permission.name = faker.lorem.word();
    permission.action = faker.random.arrayElement(PERMISSION_ACTIONS);
    permission.resource = faker.lorem.word();
    const permissionRepository =  connection.getRepository(Permission);
    expect(await permissionRepository.save(permission)).toEqual({
        name: permission.name,
        id: expect.any(Number),
        action: expect.any(String),
        resource: expect.any(String)
    })
});


it("Creates Permission with Roles", async() => {
    const permissionName = faker.lorem.word();
    const roleName = faker.lorem.word();
    const permissionRepository = connection.getRepository(Permission);
    let permission = new Permission();
    let role = new Role();
    role.name = roleName;
    permission.name = permissionName;
    permission.action = faker.random.arrayElement(PERMISSION_ACTIONS)
    permission.resource = faker.lorem.word();
    role = await role.save();
    permission.roles = Promise.resolve([role]);
    
    permission = await permission.save()
    
    expect(permission.id).toBeDefined();
    
    const dbPermission = _.first(await permissionRepository.findByIds([permission.id])) as Permission;
    

    expect(dbPermission).toBeDefined();
    expect(dbPermission).toEqual({
        name: permissionName,
        id: expect.any(Number),
        action: expect.any(String),
        resource: expect.any(String)
    })

    const roles: Role[] = await dbPermission.roles;
    expect(roles).toHaveLength(1);
    expect(roles).toBeDefined();
    expect(_.first(roles)).toEqual({
        name: roleName,
        id: expect.any(Number)
    })
});

afterAll(async() => {
    connection.close()
});