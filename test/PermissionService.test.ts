import { exportNamedDeclaration, expressionStatement } from "@babel/types";
import { Access, AccessControl } from "accesscontrol";
import * as faker from "faker";
import { Connection, createConnection } from "typeorm";
import { Permission } from "../src/entity/Permission";
import { Role } from "../src/entity/Role";
import { PermissionService } from "../src/PermissionService"

describe("Permission Service", () => {
    let connection: Connection;
    
    beforeAll(async() =>
    {
        connection = await createConnection({
            type: "sqlite",
            database: ":memory:",
            migrations: ['src/migration/*.ts'],
            entities: ['src/entity/*.ts'],
            migrationsRun: true
        });
    });
    it('Can Load All Permissions', async() => {
        const resource = faker.lorem.word();
        const roleName = faker.lorem.word();
        let role: Role =  new Role() 
        role.name = roleName;
        role = await role.save();

        let permissions: Permission[] = [
            new Permission(`CreateAny${resource}`,'create:any',resource),
            new Permission(`CreateOwn${resource}`,'create:own',resource),
            new Permission(`ReadAny${resource}`,'read:any',resource),
            new Permission(`ReadOwn${resource}`,'read:own',resource),
            new Permission(`UpdateAny${resource}`, 'update:any', resource),
            new Permission(`UpdateOwn${resource}`,'update:any', resource),
            new Permission(`DeleteAny${resource}`, 'delete:any', resource),
            new Permission(`DeleteOwn${resource}`, 'delete:own', resource),
        ];
        
        permissions.forEach((permission) => {
            permission.roles = Promise.resolve([role]);
        });
        
        permissions = await Permission.save(permissions);

        console.log(permissions);

        let permissionService = PermissionService.getInstance();
        let appPermissions = await permissionService.getAppPermissions();
        
        expect(appPermissions).toBeDefined();

        let ac = new AccessControl(appPermissions).lock();
        
        expect(ac.can(roleName).createOwn(resource)).toBeTruthy();
        expect(ac.can(roleName).createAny(resource)).toBeTruthy();
        expect(ac.can(roleName).readAny(resource)).toBeTruthy();
        expect(ac.can(roleName).readOwn(resource)).toBeTruthy();
        expect(ac.can(roleName).updateAny(resource)).toBeTruthy();
        expect(ac.can(roleName).updateOwn(resource)).toBeTruthy();
        expect(ac.can(roleName).deleteAny(resource)).toBeTruthy();
        expect(ac.can(roleName).deleteOwn(resource)).toBeTruthy();
    });

});