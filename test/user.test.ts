import { User } from "../src/entity/User";
import * as faker from 'faker';
import 'jest-extended';
import { Connection, createConnection } from "typeorm";
import { Role } from "../src/entity/Role";
import * as _ from 'lodash';

describe("User CRUD", () => {
    let goodUser: User;
    const goodUserPass: string = faker.internet.password();
    const goodUserUserName: string = faker.internet.userName();
    let connection: Connection;

    beforeAll(async () => {
        connection = await createConnection({
            type: "sqlite",
            database: ":memory:",
            migrations: ['src/migration/*.ts'],
            entities: ['src/entity/*.ts'],
            migrationsRun: true
        });
    });

    beforeEach(async () => {
        goodUser = new User();
        goodUser.userName = goodUserUserName
        await goodUser.setPassword(goodUserPass);

    });

    it("User => checkPassword Should Pass", async () => {
        let response: Promise<boolean> = goodUser.checkPassword(goodUserPass);
        expect(response).toResolve();
        try {
            let detailedResponse: boolean = await response;
            expect(detailedResponse).toBeBoolean();
            expect(detailedResponse).toBeTrue();
        }
        catch (e) {
            fail(e);
        }
    });

    it("User => Saves", async () => {
        let savedUser = await goodUser.save();
        expect(savedUser).toEqual({
            id: expect.any(Number),
            password: expect.any(String),
            userName: goodUserUserName
        });
    });

    it("User => canAssociateRoles", async () => {
        let testRole: Role = new Role();
        testRole.name = faker.lorem.word();
        testRole = await testRole.save();
        goodUser.roles = Promise.resolve([testRole]);
        let testUser = await goodUser.save();
        let testRolesPromise = testUser.roles;
        expect(testRolesPromise).toResolve();
        let testRolesArray = await testRolesPromise;
        expect(testRolesArray).toBeArrayOfSize(1);
        let actualRole = _.first(testRolesArray);
        expect(actualRole).toEqual(testRole);
    });
});