import { Permission } from '../src/entity/Permission';
import * as faker from 'faker';
import 'jest-extended';
import { Connection, createConnection } from "typeorm";
import { Role } from "../src/entity/Role";
import * as _ from 'lodash';

describe("Role CRUD", () => {
    let goodRole: Role;
    let goodRoleName: string = faker.lorem.word();
    let connection: Connection;

    beforeAll(async () => {
        connection = await createConnection({
            type: "sqlite",
            database: ":memory:",
            migrations: ['src/migration/*.ts'],
            entities: ['src/entity/*.ts'],
            migrationsRun: true
        });
    });

    beforeEach(async () => {
        goodRole = new Role();
        goodRole.name = goodRoleName;
    });

    it("Role => Saves", async () => {
        let savedRole = await goodRole.save();
        expect(savedRole).toEqual({
            id: expect.any(Number),
            name: goodRoleName
        });
    });

    it("Role => canAssociatePermissions", async () => {
        let testPermission: Permission = new Permission();
        testPermission.name = faker.lorem.word();
        testPermission.resource = faker.lorem.word();
        testPermission.action = faker.lorem.word();
        testPermission = await testPermission.save();
        goodRole.permissions = Promise.resolve([testPermission]);
        let testRole = await goodRole.save();
        let testPermissionsPromise = testRole.permissions;
        expect(testPermissionsPromise).toResolve();
        let tesmPermissionsArray = await testPermissionsPromise;
        expect(tesmPermissionsArray).toBeArrayOfSize(1);
        let actualPermission = _.first(tesmPermissionsArray);
        expect(actualPermission).toEqual(testPermission);
    });
});