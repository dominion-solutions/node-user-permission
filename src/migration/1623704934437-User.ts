import {MigrationInterface, QueryRunner, Table} from "typeorm";
import { User } from "../entity/User";

export class User1623704934437 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const userTable = new Table({
            name: "user",
            columns:[
                {
                    name: "id",
                    type: "integer",
                    isGenerated: true,
                    generationStrategy: "increment",
                    isNullable: false,
                    isPrimary: true,
                },
                {
                    name: "userName",
                    type: "varchar(255)",
                    isNullable: false,
                },
                {
                    name: "password",
                    type: "varchar(255)",
                    isNullable: false,
                },
                {
                    name: "created",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
                {
                    name: "updated",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
            ]
        });
        queryRunner.createTable(userTable);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable('user');
    }

}
