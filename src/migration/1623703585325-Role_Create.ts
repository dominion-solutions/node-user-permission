import {MigrationInterface, QueryRunner, Table} from "typeorm";
import { Role } from "../entity/Role";

export class RoleCreate1623703585325 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const roleTable: Table = new Table({
            name: "role",
            columns: [
                {
                    name: "id",
                    type: "integer",
                    unsigned: true,
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: "increment",
                    isNullable: false
                },
                {
                    name: "name",
                    type: "varchar(255)",
                    isNullable: false,
                },
                {
                    name: "created",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
                {
                    name: "updated",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
            ]
        });
        queryRunner.createTable(roleTable, true, true, true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropDatabase("roles", true);
    }

}
