import { isNull } from "lodash";
import {MigrationInterface, QueryRunner, Table, TableColumn} from "typeorm";

export class PermissionCreate1623702818464 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        const permissionTable: Table = new Table(
            {
                name: "permission",
                columns: [{
                    name: "id",
                    type: "integer",
                    isGenerated: true,
                    isPrimary: true,
                    generationStrategy: "increment",
                    unsigned: true
                },
                {
                    name: "name",
                    type: "varchar(255)",
                    isNullable: true
                },
                {
                    name: 'action',
                    type: 'varchar(16)',
                    isNullable: false
                },
                {
                    name: 'resource',
                    type: 'varchar(255)',
                    isNullable: false
                },
                {
                    name: "create_date",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },{
                    name: "update_date",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP",
                },
            ],
            }
        );
        queryRunner.createTable(permissionTable, true, true)
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable("permission", true);
    }

}
