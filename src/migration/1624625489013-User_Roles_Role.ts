import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class UserRolesRole1624625489013 implements MigrationInterface {
    readonly TABLE_NAME: string = 'user_roles_role';

    public async up(queryRunner: QueryRunner): Promise<void> {
        const userRolesRoleTable: Table = new Table({
            name: this.TABLE_NAME,
            columns: [
                {
                    name: 'id',
                    type: 'integer',
                    isGenerated: true,
                    generationStrategy: 'increment',
                    isPrimary: true,
                    isNullable: false
                },
                {
                    name: "userId",
                    type: "int",
                    unsigned: true,
                    isNullable: false
                },
                {
                    name: "roleId",
                    type: "int",
                    unsigned: true,
                    isNullable: false
                },
                {
                    name: "created",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
                {
                    name: "updated",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
            ],
            foreignKeys: [
                {
                    columnNames: ["roleId"],
                    referencedColumnNames: ["id"],
                    referencedTableName: "role"
                },
                {
                    columnNames: ["userId"],
                    referencedColumnNames: ["id"],
                    referencedTableName: "user"
                }
            ]
        });
        queryRunner.createTable(userRolesRoleTable, true, true, true);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable(this.TABLE_NAME, true, true, true);
    }

}
