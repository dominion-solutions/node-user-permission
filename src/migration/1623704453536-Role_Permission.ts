import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class RolePermission1623704453536 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const rolePermissionTable: Table = new Table({
            name: "role_permissions_permission",
            columns: [
                {
                    name: "id",
                    type: "integer",
                    unsigned: true,
                    isGenerated: true,
                    isPrimary: true,
                    generationStrategy: "increment",
                    isNullable: false
                },
                {
                    name: "roleId",
                    type: "int",
                    unsigned: true,
                    isNullable: false
                },
                {
                    name: "permissionId",
                    type: "int",
                    unsigned: true,
                    isNullable: false
                },
                {
                    name: "created",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
                {
                    name: "updated",
                    type: "timestamp",
                    default: "CURRENT_TIMESTAMP"
                },
            ],
            foreignKeys: [
                {
                    columnNames: ["roleId"],
                    referencedColumnNames: ["id"],
                    referencedTableName: "role"
                },
                {
                    columnNames: ["permissionId"],
                    referencedColumnNames: ["id"],
                    referencedTableName: "permission"
                }
            ]
        });
        queryRunner.createTable(rolePermissionTable, true, true, true) 
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        queryRunner.dropTable("role_permissions");
        
    }

}
