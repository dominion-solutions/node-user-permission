import { Permission as AccessControlPermission } from "accesscontrol";
import { BaseEntity, Column, Entity, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./Role";

@Entity()
export class Permission extends BaseEntity {
    @PrimaryGeneratedColumn()
    public id? : number |  string;
    
    @Column()
    public name: string;

    @Column()
    public action: string;

    @Column()
    public resource: string;

    @ManyToMany(type => Role, role => role.permissions)
    public roles: Promise<Role[]>;

    constructor(name?: string, action?: string, resource?: string) {
        super();
        if(name) {
            this.name = name;
        }
        if(action){
            this.action = action;
        }
        
        if(resource){
            this.resource = resource;
        }
    }
}