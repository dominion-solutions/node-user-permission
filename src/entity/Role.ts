import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Permission } from "./Permission";
import { User } from "./User";

@Entity()
export class Role extends BaseEntity{
    
    @PrimaryGeneratedColumn()
    public id?: number | string;

    @Column()
    public name: string;

    @ManyToMany(type => Permission, permission => permission.roles)
    @JoinTable()
    public permissions?: Promise<Permission[]>;

    @ManyToMany(type => User, user => user.roles)
    public users? : Promise<User[]>;
    
}