import { hash } from "bcrypt";
import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Role } from "./Role";
import * as bcrypt from "bcrypt";

@Entity()
export class User extends BaseEntity
{

    @PrimaryGeneratedColumn()
    public id?: number | string;

    @Column()
    public userName: string;

    @Column()
    private password: string

    @ManyToMany(type => Role, role => role.id)
    @JoinTable()
    public roles?: Promise<Role[]>;

    public async setPassword(password: string) 
    {
        this.password = await bcrypt.hash(password, 10);
        return Promise.resolve();
    }

    public checkPassword(plainTextPassword: string): Promise<boolean> {
        return bcrypt.compare(plainTextPassword, this.password);
    }
}
