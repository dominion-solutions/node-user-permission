import { Role, Permission } from ".";

export class PermissionService {
    public static appPermissions: Array<any> = [];
    private static instance: PermissionService;

    public static getInstance(): PermissionService {
        if (!this.instance) {
            this.instance = new PermissionService();
        }
        return this.instance;
    }

    private constructor() {
    }

    public async getAppPermissions(forceReload?: boolean): Promise<Array<any>> {
        if (forceReload || PermissionService.appPermissions.length == 0) {
            try {
                const permissions: Array<Permission> = await Permission.find();
                for(let permission of permissions) {
                    const roles = await permission.roles;
                    for (const role of roles) {
                        PermissionService.appPermissions.push({
                            role: role.name,
                            resource: permission.resource,
                            action: permission.action,
                            attributes: '*'
                        });
                    }
                }
            } catch (reason) {
                return Promise.reject(reason);
            };
        }
        return Promise.resolve(PermissionService.appPermissions);
    }
}